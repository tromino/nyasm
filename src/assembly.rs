/*
	nyasm, a simple 6502 assembler written in Rust
	Copyright (C) 2021  tromino <trominode@pm.me>
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use crate::util;

pub fn process_op_assembly(
	op: [&str; 2],
	org: &mut Box<u16>,
	label_requests: &mut Vec<(u16, String, bool)>,
	counter: u16,
	did_error: &mut bool
) -> Vec<u8> {
	let op_bin: Vec<u8>;
	
	match op[0] {
		"lda" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"literal" => {
					op_bin = vec![0xa9u8, parsed_argument.1];
				},
				"zero" => {
					op_bin = vec![0xa5u8, parsed_argument.1];
				},
				"zero,x" => {
					op_bin = vec![0xb5u8, parsed_argument.1];
				},
				"(zero,x)" => {
					op_bin = vec![0xa1u8, parsed_argument.1];
				},
				"(zero),y" => {
					op_bin = vec![0xb1u8, parsed_argument.1];
				},
				"address" => {
					op_bin = vec![0xadu8, parsed_argument.1, parsed_argument.2];
				},
				"address,x" => {
					op_bin = vec![0xbdu8, parsed_argument.1, parsed_argument.2];
				},
				"address,y" => {
					op_bin = vec![0xb9u8, parsed_argument.1, parsed_argument.2];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for lda", parsed_argument.0, did_error);
					}
				}
			}
		},
		"ldx" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"literal" => {
					op_bin = vec![0xa2u8, parsed_argument.1];
				},
				"zero" => {
					op_bin = vec![0xa6u8, parsed_argument.1];
				},
				"zero,y" => {
					op_bin = vec![0xb6u8, parsed_argument.1];
				},
				"address" => {
					op_bin = vec![0xaeu8, parsed_argument.1];
				},
				"address,y" => {
					op_bin = vec![0xbeu8, parsed_argument.1];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for ldx", parsed_argument.0, did_error);
					}
				}
			}
		},
		"ldy" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"literal" => {
					op_bin = vec![0xa0u8, parsed_argument.1];
				},
				"zero" => {
					op_bin = vec![0xa4u8, parsed_argument.1];
				},
				"zero,x" => {
					op_bin = vec![0xb4u8, parsed_argument.1];
				},
				"address" => {
					op_bin = vec![0xacu8, parsed_argument.1];
				},
				"address,x" => {
					op_bin = vec![0xbcu8, parsed_argument.1];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for ldy", parsed_argument.0, did_error);
					}
				}
			}
		},
		"sta" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"zero" => {
					op_bin = vec![0x85u8, parsed_argument.1];
				},
				"zero,x" => {
					op_bin = vec![0x95u8, parsed_argument.1];
				},
				"(zero,x)" => {
					op_bin = vec![0x81u8, parsed_argument.1];
				},
				"(zero),y" => {
					op_bin = vec![0x91u8, parsed_argument.1];
				},
				"address" => {
					op_bin = vec![0x8du8, parsed_argument.1, parsed_argument.2];
				},
				"address,x" => {
					op_bin = vec![0x9du8, parsed_argument.1, parsed_argument.2];
				},
				"address,y" => {
					op_bin = vec![0x99u8, parsed_argument.1, parsed_argument.2];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for sta", parsed_argument.0, did_error);
					}
				}
			}
		},
		"stx" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"zero" => {
					op_bin = vec![0x86u8, parsed_argument.1];
				},
				"zero,y" => {
					op_bin = vec![0x96u8, parsed_argument.1];
				},
				"address" => {
					op_bin = vec![0x8eu8, parsed_argument.1, parsed_argument.2];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for stx", parsed_argument.0, did_error);
					}
				}
			}
		},
		"sty" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"zero" => {
					op_bin = vec![0x84u8, parsed_argument.1];
				},
				"zero,x" => {
					op_bin = vec![0x94u8, parsed_argument.1];
				},
				"address" => {
					op_bin = vec![0x8cu8, parsed_argument.1, parsed_argument.2];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for sty", parsed_argument.0, did_error);
					}
				}
			}
		},
		"cmp" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"literal" => {
					op_bin = vec![0xc9u8, parsed_argument.1];
				},
				"zero" => {
					op_bin = vec![0xc5u8, parsed_argument.1];
				},
				"zero,x" => {
					op_bin = vec![0xd5u8, parsed_argument.1];
				},
				"(zero,x)" => {
					op_bin = vec![0xc1u8, parsed_argument.1];
				},
				"(zero),y" => {
					op_bin = vec![0xd1u8, parsed_argument.1];
				},
				"address" => {
					op_bin = vec![0xcdu8, parsed_argument.1, parsed_argument.2];
				},
				"address,x" => {
					op_bin = vec![0xddu8, parsed_argument.1, parsed_argument.2];
				},
				"address,y" => {
					op_bin = vec![0xd9u8, parsed_argument.1, parsed_argument.2];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for cmp", parsed_argument.0, did_error);
					}
				}
			}
		},
		"cpx" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"literal" => {
					op_bin = vec![0xe0u8, parsed_argument.1];
				},
				"zero" => {
					op_bin = vec![0xe4u8, parsed_argument.1];
				},
				"address" => {
					op_bin = vec![0xecu8, parsed_argument.1, parsed_argument.2];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for cpx", parsed_argument.0, did_error);
					}
				}
			}
		},
		"cpy" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"literal" => {
					op_bin = vec![0xc0u8, parsed_argument.1];
				},
				"zero" => {
					op_bin = vec![0xc4u8, parsed_argument.1];
				},
				"address" => {
					op_bin = vec![0xccu8, parsed_argument.1, parsed_argument.2];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for cpy", parsed_argument.0, did_error);
					}
				}
			}
		},
		"inc" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"zero" => {
					op_bin = vec![0xe6u8, parsed_argument.1];
				},
				"zero,x" => {
					op_bin = vec![0xf6u8, parsed_argument.1];
				},
				"address" => {
					op_bin = vec![0xeeu8, parsed_argument.1, parsed_argument.2];
				},
				"address,x" => {
					op_bin = vec![0xfeu8, parsed_argument.1, parsed_argument.2];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for inc", parsed_argument.0, did_error);
					}
				}
			}
		},
		"dec" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"zero" => {
					op_bin = vec![0xc6u8, parsed_argument.1];
				},
				"zero,x" => {
					op_bin = vec![0xd6u8, parsed_argument.1];
				},
				"address" => {
					op_bin = vec![0xceu8, parsed_argument.1, parsed_argument.2];
				},
				"address,x" => {
					op_bin = vec![0xdeu8, parsed_argument.1, parsed_argument.2];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for dec", parsed_argument.0, did_error);
					}
				}
			}
		},
		"adc" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"literal" => {
					op_bin = vec![0x69u8, parsed_argument.1];
				},
				"zero" => {
					op_bin = vec![0x65u8, parsed_argument.1];
				},
				"zero,x" => {
					op_bin = vec![0x75u8, parsed_argument.1];
				},
				"(zero,x)" => {
					op_bin = vec![0x61u8, parsed_argument.1];
				},
				"(zero),y" => {
					op_bin = vec![0x71u8, parsed_argument.1];
				},
				"address" => {
					op_bin = vec![0x6du8, parsed_argument.1, parsed_argument.2];
				},
				"address,x" => {
					op_bin = vec![0x7du8, parsed_argument.1, parsed_argument.2];
				},
				"address,y" => {
					op_bin = vec![0x79u8, parsed_argument.1, parsed_argument.2];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for adc", parsed_argument.0, did_error);
					}
				}
			}
		},
		"sbc" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"literal" => {
					op_bin = vec![0xe9u8, parsed_argument.1];
				},
				"zero" => {
					op_bin = vec![0xe5u8, parsed_argument.1];
				},
				"zero,x" => {
					op_bin = vec![0xf5u8, parsed_argument.1];
				},
				"(zero,x)" => {
					op_bin = vec![0xe1u8, parsed_argument.1];
				},
				"(zero),y" => {
					op_bin = vec![0xf1u8, parsed_argument.1];
				},
				"address" => {
					op_bin = vec![0xedu8, parsed_argument.1, parsed_argument.2];
				},
				"address,x" => {
					op_bin = vec![0xfdu8, parsed_argument.1, parsed_argument.2];
				},
				"address,y" => {
					op_bin = vec![0xf9u8, parsed_argument.1, parsed_argument.2];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for sbc", parsed_argument.0, did_error);
					}
				}
			}
		},
		"and" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"literal" => {
					op_bin = vec![0x29u8, parsed_argument.1];
				},
				"zero" => {
					op_bin = vec![0x25u8, parsed_argument.1];
				},
				"zero,x" => {
					op_bin = vec![0x35u8, parsed_argument.1];
				},
				"(zero,x)" => {
					op_bin = vec![0x21u8, parsed_argument.1];
				},
				"(zero),y" => {
					op_bin = vec![0x31u8, parsed_argument.1];
				},
				"address" => {
					op_bin = vec![0x2du8, parsed_argument.1, parsed_argument.2];
				},
				"address,x" => {
					op_bin = vec![0x3du8, parsed_argument.1, parsed_argument.2];
				},
				"address,y" => {
					op_bin = vec![0x39u8, parsed_argument.1, parsed_argument.2];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for and", parsed_argument.0, did_error);
					}
				}
			}
		},
		"ora" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"literal" => {
					op_bin = vec![0x09u8, parsed_argument.1];
				},
				"zero" => {
					op_bin = vec![0x05u8, parsed_argument.1];
				},
				"zero,x" => {
					op_bin = vec![0x15u8, parsed_argument.1];
				},
				"(zero,x)" => {
					op_bin = vec![0x01u8, parsed_argument.1];
				},
				"(zero),y" => {
					op_bin = vec![0x11u8, parsed_argument.1];
				},
				"address" => {
					op_bin = vec![0x0du8, parsed_argument.1, parsed_argument.2];
				},
				"address,x" => {
					op_bin = vec![0x1du8, parsed_argument.1, parsed_argument.2];
				},
				"address,y" => {
					op_bin = vec![0x19u8, parsed_argument.1, parsed_argument.2];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for ora", parsed_argument.0, did_error);
					}
				}
			}
		},
		"eor" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"literal" => {
					op_bin = vec![0x49u8, parsed_argument.1];
				},
				"zero" => {
					op_bin = vec![0x45u8, parsed_argument.1];
				},
				"zero,x" => {
					op_bin = vec![0x55u8, parsed_argument.1];
				},
				"(zero,x)" => {
					op_bin = vec![0x41u8, parsed_argument.1];
				},
				"(zero),y" => {
					op_bin = vec![0x51u8, parsed_argument.1];
				},
				"address" => {
					op_bin = vec![0x4du8, parsed_argument.1, parsed_argument.2];
				},
				"address,x" => {
					op_bin = vec![0x5du8, parsed_argument.1, parsed_argument.2];
				},
				"address,y" => {
					op_bin = vec![0x59u8, parsed_argument.1, parsed_argument.2];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for eor", parsed_argument.0, did_error);
					}
				}
			}
		},
		"bit" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"zero" => {
					op_bin = vec![0x24u8, parsed_argument.1];
				},
				"address" => {
					op_bin = vec![0x2cu8, parsed_argument.1, parsed_argument.2];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for bit", parsed_argument.0, did_error);
					}
				}
			}
		},
		"asl" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"a" => {
					op_bin = vec![0x0au8];
				},
				"zero" => {
					op_bin = vec![0x06u8, parsed_argument.1];
				},
				"zero,x" => {
					op_bin = vec![0x16u8, parsed_argument.1];
				},
				"address" => {
					op_bin = vec![0x0eu8, parsed_argument.1, parsed_argument.2];
				},
				"address,x" => {
					op_bin = vec![0x1eu8, parsed_argument.1, parsed_argument.2];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for asl", parsed_argument.0, did_error);
					}
				}
			}
		},
		"lsr" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"a" => {
					op_bin = vec![0x4au8];
				},
				"zero" => {
					op_bin = vec![0x46u8, parsed_argument.1];
				},
				"zero,x" => {
					op_bin = vec![0x56u8, parsed_argument.1];
				},
				"address" => {
					op_bin = vec![0x4eu8, parsed_argument.1, parsed_argument.2];
				},
				"address,x" => {
					op_bin = vec![0x5eu8, parsed_argument.1, parsed_argument.2];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for lsr", parsed_argument.0, did_error);
					}
				}
			}
		},
		"rol" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"a" => {
					op_bin = vec![0x2au8];
				},
				"zero" => {
					op_bin = vec![0x26u8, parsed_argument.1];
				},
				"zero,x" => {
					op_bin = vec![0x36u8, parsed_argument.1];
				},
				"address" => {
					op_bin = vec![0x2eu8, parsed_argument.1, parsed_argument.2];
				},
				"address,x" => {
					op_bin = vec![0x3eu8, parsed_argument.1, parsed_argument.2];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for rol", parsed_argument.0, did_error);
					}
				}
			}
		},
		"ror" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"a" => {
					op_bin = vec![0x6au8];
				},
				"zero" => {
					op_bin = vec![0x66u8, parsed_argument.1];
				},
				"zero,x" => {
					op_bin = vec![0x76u8, parsed_argument.1];
				},
				"address" => {
					op_bin = vec![0x6eu8, parsed_argument.1, parsed_argument.2];
				},
				"address,x" => {
					op_bin = vec![0x7eu8, parsed_argument.1, parsed_argument.2];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for ror", parsed_argument.0, did_error);
					}
				}
			}
		},
		"jmp" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"address" => {
					op_bin = vec![0x4cu8, parsed_argument.1, parsed_argument.2];
				},
				"(address)" => {
					op_bin = vec![0x6cu8, parsed_argument.1, parsed_argument.2];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for jmp", parsed_argument.0, did_error);
					}
				}
			}
		},
		"jsr" => {
			let parsed_argument = util::parse_number(op[1], false, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"address" => {
					op_bin = vec![0x20u8, parsed_argument.1, parsed_argument.2];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for jsr", parsed_argument.0, did_error);
					}
				}
			}
		},
		"bpl" => {
			let parsed_argument = util::parse_number(op[1], true, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"branch" => {
					op_bin = vec![0x10u8, parsed_argument.1];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for bpl", parsed_argument.0, did_error);
					}
				}
			}
		},
		"bmi" => {
			let parsed_argument = util::parse_number(op[1], true, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"branch" => {
					op_bin = vec![0x30u8, parsed_argument.1];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for bmi", parsed_argument.0, did_error);
					}
				}
			}
		},
		"bvc" => {
			let parsed_argument = util::parse_number(op[1], true, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"branch" => {
					op_bin = vec![0x50u8, parsed_argument.1];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for bvc", parsed_argument.0, did_error);
					}
				}
			}
		},
		"bvs" => {
			let parsed_argument = util::parse_number(op[1], true, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"branch" => {
					op_bin = vec![0x70u8, parsed_argument.1];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for bvs", parsed_argument.0, did_error);
					}
				}
			}
		},
		"bcc" => {
			let parsed_argument = util::parse_number(op[1], true, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"branch" => {
					op_bin = vec![0x90u8, parsed_argument.1];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for bcc", parsed_argument.0, did_error);
					}
				}
			}
		},
		"bcs" => {
			let parsed_argument = util::parse_number(op[1], true, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"branch" => {
					op_bin = vec![0xb0u8, parsed_argument.1];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for bcs", parsed_argument.0, did_error);
					}
				}
			}
		},
		"bne" => {
			let parsed_argument = util::parse_number(op[1], true, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"branch" => {
					op_bin = vec![0xd0u8, parsed_argument.1];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for bne", parsed_argument.0, did_error);
					}
				}
			}
		},
		"beq" => {
			let parsed_argument = util::parse_number(op[1], true, true, label_requests, org, counter + 1, did_error);
			
			match parsed_argument.0 {
				"branch" => {
					op_bin = vec![0xf0u8, parsed_argument.1];
				},
				_ => {
					op_bin = vec![];
					
					if parsed_argument.0 != "" {
						util::assembler_error("Argument type not valid for beq", parsed_argument.0, did_error);
					}
				}
			}
		},
		"tax" => {
			if op[1] == "" {
				op_bin = vec![0xaau8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to tax, which is an implicit instruction", op[1], did_error);
			}
		},
		"txa" => {
			if op[1] == "" {
				op_bin = vec![0x8au8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to txa, which is an implicit instruction", op[1], did_error);
			}
		},
		"inx" => {
			if op[1] == "" {
				op_bin = vec![0xe8u8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to inx, which is an implicit instruction", op[1], did_error);
			}
		},
		"dex" => {
			if op[1] == "" {
				op_bin = vec![0xcau8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to dex, which is an implicit instruction", op[1], did_error);
			}
		},
		"tay" => {
			if op[1] == "" {
				op_bin = vec![0xa8u8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to tay, which is an implicit instruction", op[1], did_error);
			}
		},
		"tya" => {
			if op[1] == "" {
				op_bin = vec![0x98u8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to tya, which is an implicit instruction", op[1], did_error);
			}
		},
		"iny" => {
			if op[1] == "" {
				op_bin = vec![0xc8u8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to iny, which is an implicit instruction", op[1], did_error);
			}
		},
		"dey" => {
			if op[1] == "" {
				op_bin = vec![0x88u8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to dey, which is an implicit instruction", op[1], did_error);
			}
		},
		"rti" => {
			if op[1] == "" {
				op_bin = vec![0x40u8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to rti, which is an implicit instruction", op[1], did_error);
			}
		},
		"rts" => {
			if op[1] == "" {
				op_bin = vec![0x60u8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to rts, which is an implicit instruction", op[1], did_error);
			}
		},
		"txs" => {
			if op[1] == "" {
				op_bin = vec![0x9au8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to txs, which is an implicit instruction", op[1], did_error);
			}
		},
		"tsx" => {
			if op[1] == "" {
				op_bin = vec![0xbau8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to tsx, which is an implicit instruction", op[1], did_error);
			}
		},
		"pha" => {
			if op[1] == "" {
				op_bin = vec![0x48u8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to pha, which is an implicit instruction", op[1], did_error);
			}
		},
		"pla" => {
			if op[1] == "" {
				op_bin = vec![0x68u8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to pla, which is an implicit instruction", op[1], did_error);
			}
		},
		"php" => {
			if op[1] == "" {
				op_bin = vec![0x08u8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to php, which is an implicit instruction", op[1], did_error);
			}
		},
		"plp" => {
			if op[1] == "" {
				op_bin = vec![0x28u8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to plp, which is an implicit instruction", op[1], did_error);
			}
		},
		"clc" => {
			if op[1] == "" {
				op_bin = vec![0x18u8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to clc, which is an implicit instruction", op[1], did_error);
			}
		},
		"sec" => {
			if op[1] == "" {
				op_bin = vec![0x38u8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to sec, which is an implicit instruction", op[1], did_error);
			}
		},
		"cli" => {
			if op[1] == "" {
				op_bin = vec![0x58u8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to cli, which is an implicit instruction", op[1], did_error);
			}
		},
		"sei" => {
			if op[1] == "" {
				op_bin = vec![0x78u8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to sei, which is an implicit instruction", op[1], did_error);
			}
		},
		"clv" => {
			if op[1] == "" {
				op_bin = vec![0xb8u8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to clv, which is an implicit instruction", op[1], did_error);
			}
		},
		"cld" => {
			if op[1] == "" {
				op_bin = vec![0xd8u8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to cld, which is an implicit instruction", op[1], did_error);
			}
		},
		"sed" => {
			if op[1] == "" {
				op_bin = vec![0xf8u8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to sed, which is an implicit instruction", op[1], did_error);
			}
		},
		"brk" => {
			if op[1] == "" {
				op_bin = vec![0x00u8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to brk, which is an implicit instruction", op[1], did_error);
			}
		},
		"nop" => {
			if op[1] == "" {
				op_bin = vec![0xeau8];
			} else {
				op_bin = vec![];
				
				util::assembler_error("Passed argument to nop, which is an implicit instruction", op[1], did_error);
			}
		},
		_ => {
			op_bin = vec![];
			util::assembler_error("Unknown instruction", op[0], did_error);
		}
	}
	
	return op_bin;
}
