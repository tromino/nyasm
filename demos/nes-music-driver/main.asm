	; Origin for addresses ($8000 - header size)
	.org $7ff0

; ----- Header -----

	.byte $4e,$45,$53,$1a,$01,$01,$00,$08
	.pad $0010

; ----- Pitch Tables -----

table1:
	.byte $00,$00
	
	.byte $f1,$7f,$13,$ad,$4d,$f3,$9d,$4c
	.byte $00,$b8,$74,$34,$f8,$bf,$89,$56
	.byte $26,$f9,$ce,$a6,$80,$5c,$3a,$1a
	.byte $fb,$df,$c4,$ab,$93,$7c,$67,$52
	.byte $3f,$2d,$1c,$0c,$fd,$ef,$e1,$d5
	.byte $c9,$bd,$b3,$a9,$9f,$96,$8e,$86
	.byte $7e,$77,$70,$6a,$64,$5e,$59,$54
	.byte $4f,$4b,$46,$42,$3f,$3b,$38,$34
	.byte $31,$2f,$2c,$29,$27,$25,$23,$21
	.byte $1f,$1d,$1b,$1a,$18,$17,$15,$14

table2:
	.byte $00,$00
	
	.byte $07,$07,$07,$06,$06,$05,$05,$05
	.byte $05,$04,$04,$04,$03,$03,$03,$03
	.byte $03,$02,$02,$02,$02,$02,$02,$02
	.byte $01,$01,$01,$01,$01,$01,$01,$01
	.byte $01,$01,$01,$01,$00,$00,$00,$00
	.byte $00,$00,$00,$00,$00,$00,$00,$00
	.byte $00,$00,$00,$00,$00,$00,$00,$00
	.byte $00,$00,$00,$00,$00,$00,$00,$00
	.byte $00,$00,$00,$00,$00,$00,$00,$00
	.byte $00,$00,$00,$00,$00,$00,$00,$00

; ----- Song Data -----

song_demo:
	.addr song_demo
	.byte $05,$60,%01111011,%10110111,%10000001,%00010111
	.addr song_demo_pulse1,song_demo_pulse2,song_demo_triangle,song_demo_noise

song_demo_pulse1:
	.byte $00,$01,$01,$01,$01,$01,$2b,$2c,$01,$01,$01,$01
	.byte $2a,$01,$00,$27,$01,$00,$25,$01,$27,$20,$01,$01
	.byte $01,$01,$01,$1b,$01,$1e,$20,$01,$01,$1b,$01,$1e
	.byte $22,$01,$23,$00,$01,$22,$01,$01,$01,$20,$01,$01
	.byte $00,$01,$01,$2b,$2c,$01,$2b,$01,$00,$27,$01,$00
	.byte $2a,$01,$00,$27,$01,$00,$2f,$01,$31,$2c,$01,$01
	.byte $01,$01,$27,$25,$01,$23,$1e,$01,$20,$00,$01,$27
	.byte $25,$01,$23,$25,$01,$20,$00,$01,$1e,$20,$01,$01

song_demo_pulse2:
	.byte $14,$01,$01,$00,$01,$18,$01,$01,$00,$1b,$01,$00
	.byte $10,$01,$01,$00,$01,$14,$01,$01,$00,$17,$01,$00
	.byte $12,$01,$01,$00,$01,$16,$01,$01,$00,$19,$01,$00
	.byte $14,$01,$01,$01,$01,$00,$14,$01,$00,$0f,$01,$00
	.byte $14,$01,$01,$00,$01,$18,$01,$01,$00,$1b,$01,$00
	.byte $10,$01,$01,$00,$01,$14,$01,$01,$00,$17,$01,$00
	.byte $12,$01,$01,$00,$01,$16,$01,$01,$00,$19,$01,$00
	.byte $14,$01,$01,$01,$01,$00,$14,$01,$00,$0f,$01,$00

song_demo_triangle:
	.byte $14,$01,$01,$20,$00,$20,$14,$01,$01,$20,$00,$20
	.byte $10,$01,$01,$1c,$00,$1c,$10,$01,$01,$1c,$00,$1c
	.byte $12,$01,$01,$1e,$01,$1e,$12,$01,$01,$1e,$01,$1e
	.byte $14,$01,$01,$20,$00,$20,$14,$01,$01,$20,$00,$20
	.byte $14,$01,$01,$20,$00,$20,$14,$01,$01,$20,$00,$20
	.byte $10,$01,$01,$1c,$00,$1c,$10,$01,$01,$1c,$00,$1c
	.byte $12,$01,$01,$1e,$01,$1e,$12,$01,$01,$1e,$01,$1e
	.byte $14,$01,$01,$20,$00,$20,$14,$01,$01,$20,$00,$20

song_demo_noise:
	.byte $4e,$01,$01,$4e,$01,$4e,$45,$01,$01,$4e,$01,$4e
	.byte $4e,$01,$01,$4e,$01,$4e,$45,$01,$01,$4e,$01,$4e
	.byte $4e,$01,$01,$4e,$01,$4e,$45,$01,$01,$4e,$01,$4e
	.byte $4e,$01,$01,$4e,$01,$4e,$45,$01,$01,$4e,$01,$4e
	.byte $4e,$01,$01,$4e,$01,$4e,$45,$01,$01,$4e,$01,$4e
	.byte $4e,$01,$01,$4e,$01,$4e,$45,$01,$01,$4e,$01,$4e
	.byte $4e,$01,$01,$4e,$01,$4e,$45,$01,$01,$4e,$01,$4e
	.byte $4e,$01,$01,$4e,$01,$4e,$45,$01,$01,$4e,$01,$4e

; ----- Initialization -----

reset:
	sei
	cld
	
	ldx #$40
	stx $4017
	
	ldx #$ff
	txs
	
	inx
	stx $2000
	stx $2001
	stx $4010

vwait1:
	bit $2002
	bpl vwait1
	
	txa
	tay

setmem1:
	sta $00,x
	sta $0100,x
	sta $0300,x
	sta $0400,x
	sta $0500,x
	sta $0600,x
	sta $0700,x
	
	inx
	bne setmem1
	
	lda #$ff

setmem2:
	sta $0200,x
	
	inx
	bne setmem2
	
	lda #%00001111
	sta $4015
	
	lda #$00

setaudio:
	sta $4000,x
	
	inx
	cpx #$10
	bne setaudio

vwait2:
	bit $2002
	bpl vwait2
	
	lda #%10000000
	sta $2000

; ----- Main Stuff -----

	; Selecting the song to play
	lda song_demo
	sta $00
	
	ldx #$01
	lda song_demo,x
	sta $01
	
	; Play it!
	jsr playsong

forever:
	bvc forever

; ----- Player Loop -----

nmi:
	ldx $fe
	ldy $ff
	
	inx
	cpx $f0
	bne nmimid
	
	ldx #$00
	
	iny
	cpy $f1
	bne nmimid
	
	ldy #$00

nmimid:
	stx $fe
	sty $ff
	
	cpx #$00
	bne irq
	
	lda #$01
	cmp ($f2),y
	beq nmipulse2
	
	lda ($f2),y
	tax
	lda table1,x
	sta $4002
	lda table2,x
	sta $4003
	
	lda #$01

nmipulse2:
	cmp ($f4),y
	beq nmitriangle
	
	lda ($f4),y
	tax
	lda table1,x
	sta $4006
	lda table2,x
	sta $4007
	
	lda #$01

nmitriangle:
	cmp ($f6),y
	beq nminoise
	
	lda ($f6),y
	tax
	lda table1,x
	sta $400a
	lda table2,x
	sta $400b

nminoise:
	lda #%01000000
	and ($f8),y
	beq irq
	
	lda ($f8),y
	sta $400e
	lda #$00
	sta $400f

irq:
	rti

; ----- Play Subroutine -----

playsong:
	ldy #$02
	lda ($00),y
	sta $f0
	
	iny
	lda ($00),y
	sta $f1
	
	iny
	lda ($00),y
	sta $4000
	
	iny
	lda ($00),y
	sta $4004
	
	iny
	lda ($00),y
	sta $4008
	
	iny
	lda ($00),y
	sta $400c

playsongloop:
	iny
	lda ($00),y
	sta $00ea,y
	
	cpy #$0f
	bne playsongloop
	
	lda #$ff
	sta $fe
	lda #$00
	sta $ff
	
	rts

; ----- The End -----

	; Pad until start of vector space ($4000 + header size - vector space size)
	.pad $400a
	
	; The vectors themselves
	.addr nmi,reset,irq

; ----- CHR-ROM -----

	; Pad until end of CHR-ROM ($6000 + header size)
	.pad $6010
