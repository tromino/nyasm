# nes-music-driver

This is an example of a basic NES music player that plays a short 8-bar loop on
repeat.

To assemble it using nyasm:

	$ nyasm main.asm -o main.nes
