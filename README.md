# nyasm

The most awesomest 6502 assembler you've ever seen in your life!

nyasm is a toy 6502 assembler I made in order to learn Rust. It's currently
pretty early on and there will probably be breaking changes in the future. Right
now it's in a state where it can successfully assemble a valid NES ROM file,
although some features might still be incomplete or missing entirely (including
external binary files is possible now, but including other assembly files isn't
for example).

## Building

nyasm can be built using `cargo`:

	$ cargo build --release

The resulting binary will appear at `target/release/nyasm`.

## Usage

To assemble a file using nyasm, give the input and output filenames to the
`nyasm` binary like this:

	$ nyasm input.asm -o output.nes

You can also pipe data into and out of the assembler without accessing the
filesystem directly:

	$ nyasm < input.asm > output.nes

See [the demos folder](./demos/) for examples of what nyasm can do currently.

## Supported Syntax

You can use the `label_name:` syntax to declare a label:

	my_label:
		jmp my_label

If you want, you can also use the `.label` directive instead, with no
difference in behavior:

	.label my_label
		jmp my_label

Comments can be written using a semicolon:

	; This is where the magic begins...
	reset:
		lda #$2a ; 42 in hex!

Directives with multiple parameters have them separated with a comma:

	reset:
		.addr reset,reset,reset ; Writes the address of the reset label 3 times
		.pad $4000,$2a

Directives with string values should have them wrapped in quotes to preserve
casing:

	dialog_text:
		.incbin "includes/dialog.txt"

Normal instructions and directives are case insensitive:

	RESET:
		.BYTE $01,$23,$AB,$CD

## Assembler Directives

Here's a list of the assembler directives that nyasm currently supports:

### `.byte`

Places one or more 8-bit values into the assembled binary:

	.byte $00,$ff,$00,$ff

### `.addr`

Same as `.byte`, but for 16-bit values:

	.addr $0000,$ffff,my_label

### `.label`

An alternative syntax for declaring labels:

	.label my_label

### `.org`

Sets the "origin point" for labels, or where the first byte in the assembled
file would map to in the CPU's address space:

	.org $7ff0 ; The beginning of the NES PRG-ROM minus the 16-byte header

### `.pad`

Pads the assembled file with zeros up until the specified address:

	.pad $2000

You can also provide a custom byte to pad with by setting the second parameter:

	.pad $2000,$2a

### `.incbin`

Includes the specified file as binary data in the assembled file:

	.incbin "music.bin"

All paths given are relative to the currently navigated directory in the
terminal emulator.
